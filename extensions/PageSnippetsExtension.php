<?php

class PageSnippetsExtension extends DataExtension {

	private static $db = array(
		'ShowSubPagesAsSnippets' => 'Boolean'
	);

	private static $has_many = array(
		'PageSnippets' => 'PageSnippet'
	);

	public function updateCMSFields(FieldList $fields) {
		$fields->addFieldToTab('Root.Snippets', new CheckboxField('ShowSubPagesAsSnippets', 'Show Subpages as Snippets on Page'));

		$SortObject = class_exists('GridFieldSortableRows') ? new GridFieldSortableRows('SortID') : (class_exists('GridFieldOrderableRows') ? new GridFieldOrderableRows('SortID') : null);
		$gridFieldConfig = GridFieldConfig::create()->addComponents(
			new GridFieldToolbarHeader(),
			new GridFieldAddNewButton('toolbar-header-right'),
			$SortObject && class_exists('GridFieldTitleHeader') ? new GridFieldTitleHeader() : new GridFieldSortableHeader(),
			new GridFieldDataColumns(),
			new GridFieldPaginator(20),
			new GridFieldEditButton(),
			new GridFieldDeleteAction(),
			new GridFieldDetailForm(),
			$SortObject
		);
		$gridField = new GridField('PageSnippets', 'Snippets', $this->owner->PageSnippets(), $gridFieldConfig);
		$fields->addFieldToTab('Root.Snippets', $gridField);
	}

	public function SubPagesAsSnippets() {
		// create empty list
		$Snippets = ArrayList::create();

		// ad children
		foreach ($this->owner->AllChildren() as $Child) {
			// create empty snippet
			$Snippet = array();

			// title & subtitle
			$Snippet['Title'] = isset($Child->Title) ? $Child->Title : null;
			if(Config::inst()->get('PageSnippet', 'use_subtitle')) {
				$Snippet['SubTitle'] = isset($Child->SubTitle) ? $Child->SubTitle : null;
			}

			// eval content (if introtext, use that, if not, use content)
			$Content = isset($Child->IntroText) && $Child->IntroText ? $Child->IntroText : null;
			if(is_null($Content)) {
				$Content = $Child->Content;
			}
			$Snippet['Content'] = $Content;

			// eval image (look for image, introimage, galleryimages)
			$Image = isset($Child->ImageID) && $Child->ImageID ? $Child->Image() : null;
			if(is_null($Image)) {
				$Image = isset($Child->IntroImageID) && $Child->IntroImageID ? $Child->IntroImage() : null;
			}
			if(is_null($Image) && $Child->getRelationClass('GalleryImages') && $GalleryImages = $Child->GalleryImages()) {
				$Image = $GalleryImages->Count() ? $Child->GalleryImages()->First() : null;
			}
			if(is_null($Image) && $Child->getRelationClass('PageImages') && $PageImages = $Child->PageImages()) {
				$Image = $PageImages->Count() ? $Child->PageImages()->First() : null;
			}
			if(is_null($Image) && $Child->getRelationClass('Media') && $Media = $Child->Media()) {
				$Image = $Media->Count() ? $Media->First()->Image() : null;
			}
			$Snippet['Image'] = $Image;
			$Snippet['SnippetImage'] = $Image;

            // add link object
            $Snippet['Link'] = $Child;

            // add pagelink object
            $Snippet['PageLink'] = $Child->Link();

			// allow extensions
			$this->owner->extend('updateSubPageAsSnippet', $Snippet);

            // add to list
			$Snippets->push(ArrayData::create($Snippet));
		}

		return $Snippets;
	}

	public function PageSnippetList() {
		// get snippets
		$Snippets = $this->owner->PageSnippets();

		// add list of children to snippets
		if($this->owner->ShowSubPagesAsSnippets) {

			// first, create arraylist out of hasmanylist
			$List = ArrayList::create();
			foreach ($Snippets as $Snippet) {
				$List->push($Snippet);
			}

			// add kids
			$List->merge($this->SubPagesAsSnippets());
			$Snippets = $List;
			unset($List);
		}

		// return list
		return $Snippets;
 	}

}
