<?php

class PageSnippet extends DataObject {

	private static $db = array(
		'Title' => 'Varchar(255)',
		'SubTitle' => 'Varchar(255)',
		'Content' => 'HTMLText',
		'TextContent' => 'Text',
		'ExternalLink' => 'Varchar(255)',
		'SortID' => 'Int'
	);

	private static $has_one = array(
		'Link' => 'SiteTree',
		'SnippetPage' => 'SiteTree',
		'Image' => 'Image',
		'Page' => 'Page'
	);

	private static $casting = array(
		'SnippetImage' => 'Image'
	);

	private static $default_sort = 'SortID';

	public function getCMSFields() {
		$fields = parent::getCMSFields();
		if(!$this->ID || $this->SnippetPage()->exists()) {
			$fields->addFieldToTab('Root.Main', new TreeDropdownField('SnippetPageID', 'Page to display as Snippet', 'SiteTree'), 'Title');
			if(!$this->ID) {
				$fields->addFieldToTab('Root.Main', new LiteralField('OR', 'Choose an Item from the Site Tree above to display as this Snippet <strong>OR</strong> fill out the form below.'), 'Title');
			}
			if($this->SnippetPage()->exists()) {
				$fields->removeByName('Title');
				$fields->removeByName('SubTitle');
				$fields->removeByName('Content');
				$fields->removeByName('TextContent');
				$fields->removeByName('ExternalLink');
				$fields->removeByName('LinkID');
				$fields->removeByName('Image');
			} else {
				$fields->removeByName('Content');
				$fields->removeByName('TextContent');
				$fields->removeByName('LinkID');
				$fields->insertAfter(new TreeDropdownField('LinkID', 'Page to link to', 'SiteTree'), 'Title');
				$fields->addFieldToTab('Root.Main', $this->GetContentFormField());
			}
		} else {
			$fields->removeByName('SnippetPageID');
			$fields->removeByName('Content');
			$fields->removeByName('TextContent');
			$fields->addFieldToTab('Root.Main', $this->GetContentFormField());
			$fields->addFieldToTab('Root.Main', new TextField('ExternalLink'));
			$fields->addFieldToTab('Root.Main', new TreeDropdownField('LinkID', 'Page to link to', 'SiteTree'));
			$fields->addFieldToTab('Root.Main', new LiteralField('OR', 'Insert a link to an external page OR pick an element from the Site Tree below.'), 'ExternalLink');
		}

		if(!$this->config()->get('use_subtitle')) {
			$fields->removeByName('SubTitle');
		}
		$fields->removeByName('SortID');
		$fields->removeByName('PageID');
		return $fields;
	}

	public function GetContentFormField() {
		if($this->config()->get('use_html_content')) {
			$Field = HtmlEditorField::create('Content');
		} else {
			$Field = TextareaField::create('TextContent');
		}
		return $Field;
	}

	public function GetContentData() {
		if($this->config()->get('use_html_content')) {
			$Data = isset($this->record['Content']) ? $this->record['Content'] : '';
		} else {
			$Data = $this->obj('TextContent');
		}
		return $Data;
	}

	public function SnippetImage() {
		return $this->SnippetPage()->exists() && isset($this->SnippetPage()->IntroImageID) ? $this->SnippetPage()->IntroImage() : (is_object($this->Image) ? $this->Image : $this->Image());
	}

	public function getTitle() {
		return $this->SnippetPage()->exists() ? $this->SnippetPage()->Title : $this->getField('Title');
	}

	public function getSubTitle() {
		return $this->SnippetPage()->exists() && isset($this->SnippetPage()->SubTitle) ? $this->SnippetPage()->SubTitle : $this->getField('SubTitle');
	}

	public function getContent() {
		return $this->SnippetPage()->exists() ? (isset($this->SnippetPage()->IntroText) && $this->SnippetPage()->IntroText ? $this->SnippetPage()->IntroText : $this->SnippetPage()->Content) : $this->GetContentData();
	}

	public function PageObject() {
		$Page = $this->SnippetPage();
		if(!$Page->exists()) {
			$Page = is_object($this->Link) ? $this->Link : $this->Link();
		}
		return $Page;
	}

	public function PageLink() {
		$Page = $this->PageObject();
		$this->ExternalLink = trim($this->ExternalLink);
		if($Page->exists()) {
			return $Page->Link();
		} elseif($this->ExternalLink) {
			return (preg_match('/^(http|https):\/\//', $this->ExternalLink) ? '' : 'http://').$this->ExternalLink;
		}
	}

	public function LinkTitle() {
		if($this->Title) {
			return $this->Title;
		} elseif($Page = $this->SnippetPage()) {
			return $Page->Title;
		}if($Page = $this->Link()) {
			return $Page->Title;
		}
	}

}